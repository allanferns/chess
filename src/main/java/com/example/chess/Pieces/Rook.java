package com.example.chess.Pieces;

import javafx.scene.image.Image;

public class Rook extends Piece{

    public Rook(int xPosition, int yPosition, boolean isWhite, Image image, String type) {
        super(xPosition, yPosition, isWhite, image, type);
    }
}
