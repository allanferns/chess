package com.example.chess.Pieces;

import javafx.scene.image.Image;

public class Knight extends Piece{
    public Knight(int xPosition, int yPosition, boolean isWhite, Image image, String type) {
        super(xPosition, yPosition, isWhite, image, type);
    }
}
