package com.example.chess.Pieces;

import javafx.scene.image.Image;

public class Piece {
    private int xPosition;
    private int yPosition;
    private boolean isWhite;
    private Image image;
    private String type;

    public Piece(int xPosition, int yPosition, boolean isWhite, Image image, String type){
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.isWhite = isWhite;
        this.image = image;
        this.type = type;
    }

    public void moveTo(int xPosition, int yPosition){
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }

    public int getxPosition(){
        return xPosition;
    }

    public int getyPosition(){
        return yPosition;
    }

    public boolean isItWhite(){
        return isWhite;
    }

    public Image getImage(){
        return image;
    }


    public String getType(){
        return this.type;
    }



}
