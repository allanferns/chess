package com.example.chess;
import com.example.chess.Pieces.*;
import com.example.chess.domain.Board;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import java.util.ArrayList;


public class Controller{
     Board b;
     GraphicsContext gc;
     ArrayList<Piece> whitePieces;
     ArrayList<Piece> blackPieces;
     int xPosition;
     int yPosition;
     Piece currentPiece;
     boolean pieceIsSelected;
     boolean whitesTurn;

    @FXML
    Canvas canvas;

    @FXML
    Label winnerLabel;

    @FXML
    private void initialize(){
        whitesTurn = true;
        pieceIsSelected = false;
        gc = canvas.getGraphicsContext2D();
        blackPieces = new ArrayList<>();
        whitePieces = new ArrayList<>();
        b = new Board(640);
        drawBoard();
        constructPieces();
        drawPieces();
        canvas.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {


                if(!isWhiteKingAlive() || !isBlackKingAlive()){
                    return;
                }

                //Get the coordinates of the click
                if(pieceIsSelected == false){
                    currentPiece = getPiece(event);
                    try{
                        highlightOptions(currentPiece);
                    }catch (Exception e){
                        System.out.println("invalid highlight");
                    }
/*
                    if(currentPiece.getType().equals("King")){ //Highlight all enemy options when you have selected the king
                        System.out.println("in here");
                        //Highlight every enemy option
                        if(currentPiece.isItWhite()){
                            for (int i = 0; i < blackPieces.size(); i++) {
                                highlightOptions(blackPieces.get(i));
                            }
                        }else{
                            for (int i = 0; i < whitePieces.size(); i++) {
                                highlightOptions(whitePieces.get(i));
                            }
                        }
                    }
*/
                }else{
                    boolean successfulKill = false;
                    xPosition = ((int)event.getX())/80;
                    yPosition = ((int)event.getY())/80;
                    //if there is already a piece at that position, kill it
                    if(isPieceAt(xPosition, yPosition)){ // kill only a piece if there is a piece at the target. Otherwise it tries to kill a piece which isn't there
                        successfulKill =  kill(getPieceAt(xPosition, yPosition));
                        if(successfulKill){
                            currentPiece.moveTo(xPosition, yPosition);
                        }
                    }
                    if(!isPieceAt(xPosition, yPosition) && !successfulKill){
                        currentPiece.moveTo(xPosition, yPosition);
                    }
                    gc.clearRect(0,0,640,640);
                    drawBoard();
                    drawPieces();
                    pieceIsSelected = false;
                    //System.out.println("is it white turn next? " + !whitesTurn);
                    //whitesTurn = !whitesTurn;
                    if(!isBlackKingAlive()){
                        winnerLabel.setText("White wins");
                    } else if(!isWhiteKingAlive()){
                        winnerLabel.setText("Black wins");
                    }
                }
            }
        });
    }

    private Piece getPiece(MouseEvent event){
        //If button clicked on a black piece, get the piece
        xPosition = ((int)event.getX())/80;
        yPosition = ((int)event.getY())/80;
        for (int i = 0; i < blackPieces.size(); i++) {
            if(blackPieces.get(i).getxPosition() == xPosition && blackPieces.get(i).getyPosition() == yPosition){
                pieceIsSelected = true;
                return blackPieces.get(i);
            }
        }
        //if button is clicked on a white piece, get the piece
        for (int i = 0; i < whitePieces.size(); i++) {
            if(whitePieces.get(i).getxPosition() == xPosition && whitePieces.get(i).getyPosition() == yPosition){
                pieceIsSelected = true;
                return whitePieces.get(i);
            }
        }
        pieceIsSelected = false;
        return null;
    }

    private void drawBoard(){
        Color customBlack = Color.rgb(100,100,100,1);
        gc.setFill(customBlack);
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if(i%2 == 1 && j%2 == 0 || i%2 == 0 && j%2 == 1){
                    gc.fillRect(i*80,j*80,80,80);
                }
            }
        }
    }



    private void constructPieces(){
        //Creating and adding all the black and white pawns to the array
        for (int i = 0; i < 8; i++) {
            blackPieces.add(new Pawn(i, 6, false, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/Black_Pawn.png"), "Pawn"));
            whitePieces.add(new Pawn(i, 1, true, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/White_Pawn.png"), "Pawn"));
        }
        //creating the kings for both teams
        blackPieces.add(new King(4, 7, false, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/Black_King.png"), "King"));
        whitePieces.add(new King(4, 0, true, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/White_king.png"), "King"));

        //creating the white rooks
        blackPieces.add(new Rook(0, 7, false, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/Black_Rook.png"), "Rook"));
        blackPieces.add(new Rook(7, 7, false, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/Black_Rook.png"), "Rook"));

        //Creating the black Rooks
        whitePieces.add(new Rook(0, 0, true, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/White_Rook.png"), "Rook"));
        whitePieces.add(new Rook(7, 0, true, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/White_Rook.png"), "Rook"));

        //Creating the Knights
        blackPieces.add(new Knight(1, 7, false, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/Black_knight.png"), "Knight"));
        blackPieces.add(new Knight(6, 7, false, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/Black_knight.png"), "Knight"));

        whitePieces.add(new Knight(1, 0, true, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/White_Knight.png"), "Knight"));
        whitePieces.add(new Knight(6, 0, true, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/White_Knight.png"), "Knight"));

        //Creating the bishops
        blackPieces.add(new Bishop(2, 7, false, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/Black_Bishop.png"), "Bishop"));
        blackPieces.add(new Bishop(5, 7, false, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/Black_Bishop.png"), "Bishop"));

        whitePieces.add(new Bishop(2, 0, true, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/White_Bishop.png"), "Bishop"));
        whitePieces.add(new Bishop(5, 0, true, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/White_Bishop.png"), "Bishop"));

        //Creating the Queens
        blackPieces.add(new Queen(3, 7, false, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/Black_Queen.png"), "Queen"));

        whitePieces.add(new Queen(3, 0, true, new Image("file:/Users/allanfernandes/IdeaProjects/CHess/Chess/src/main/java/com/example/chess/Resources/White_Queen.png"), "Queen"));
    }

    private void drawPieces(){
        //Draw all the black pieces
        for (int i = 0; i < blackPieces.size(); i++) {
            gc.drawImage(blackPieces.get(i).getImage(), blackPieces.get(i).getxPosition()*80+9, blackPieces.get(i).getyPosition()*80+9);
        }
        //draw all the white pieces
        for (int i = 0; i < whitePieces.size(); i++) {
            gc.drawImage(whitePieces.get(i).getImage(), whitePieces.get(i).getxPosition()*80+9, whitePieces.get(i).getyPosition()*80+9);

        }
    }

    public void highlightOptions(Piece piece){
        if(piece.getType().equals("Pawn")){
            pawnOptions(piece);
        }
        if(piece.getType().equals("Rook")){
            rookOptions(piece);
        }

        if(piece.getType().equals("Bishop")){
            bishopOptions(piece);
        }
        if(piece.getType().equals("Queen")){
            queenOptions(piece);
        }
        //KING
        if(piece.getType().equals("King")){
            kingOptions(piece);
        }
        //KNIGHT
        if(piece.getType().equals("Knight")){
            knightOptions(piece);
        }
    }

    public Piece getPieceAt(int xPosition, int yPosition){
        //Scanning the blackpieces
        for (int i = 0; i < blackPieces.size(); i++) {
            if(blackPieces.get(i).getxPosition() == xPosition && blackPieces.get(i).getyPosition() == yPosition){
                return blackPieces.get(i);
            }
        }
        //scanning the whitepieces
        for (int i = 0; i < whitePieces.size(); i++) {
            if(whitePieces.get(i).getxPosition() == xPosition && whitePieces.get(i).getyPosition() == yPosition){
                return whitePieces.get(i);
            }
        }
        return null;
    }

    public boolean isPieceAt(int xPosition, int yPosition){
        //Scanning the blackpieces
        for (int i = 0; i < blackPieces.size(); i++) {
            if(blackPieces.get(i).getxPosition() == xPosition && blackPieces.get(i).getyPosition() == yPosition){
                return true;
            }
        }
        //scanning the whitepieces
        for (int i = 0; i < whitePieces.size(); i++) {
            if(whitePieces.get(i).getxPosition() == xPosition && whitePieces.get(i).getyPosition() == yPosition){
                return true;
            }
        }
        return false;
    }

    public void highLightEnemy(int x, int y){
        Color highlight = Color.rgb(241,0,0,0.2);
        gc.setFill(highlight);
        gc.fillRect(x*80, y*80, 80,80);
    }

    public void highlightSquare(int x, int y){
        Color highlight = Color.rgb(241,231,64,0.5);
        gc.setFill(highlight);
        gc.fillRect(x*80, y*80, 80,80);
    }

    public boolean kill(Piece piece){
        if(piece.equals(currentPiece)){
            System.out.println("Don't kill yourself");
            return false;
        }
        if(piece.isItWhite() && currentPiece.isItWhite()){
            System.out.println("No friendly fire");
            return false;
        }else if(!piece.isItWhite() && !currentPiece.isItWhite()){
            System.out.println("No friendly fire");
            return false;
        }
        if(piece.isItWhite()){
            whitePieces.remove(piece);
            whitePieces.trimToSize();
        }else{
            blackPieces.remove(piece);
            blackPieces.trimToSize();
        }
        return true;
    }

    public void pawnOptions(Piece piece){
        if(piece.isItWhite()){
            if(piece.getyPosition() == 1){ //starting position for whites
                if(!isPieceAt(piece.getxPosition(), piece.getyPosition()+1)){
                    highlightSquare(piece.getxPosition(), piece.getyPosition()+1);
                    if(!isPieceAt(piece.getxPosition(), piece.getyPosition()+2)){
                        highlightSquare(piece.getxPosition(), piece.getyPosition()+2);
                    }
                }
            }else{
                //if there is one obstacle 1 square away
                if(!isPieceAt(piece.getxPosition(), piece.getyPosition()+1)){
                    highlightSquare(piece.getxPosition(), piece.getyPosition()+1);
                }

            }
            //if there are enemies one diagonal away
            if(isPieceAt(piece.getxPosition()-1, piece.getyPosition()+1) && !getPieceAt(piece.getxPosition() -1, piece.getyPosition()+1).isItWhite()){ // have to also be the opposite colour
                highLightEnemy(piece.getxPosition()-1,piece.getyPosition()+1);
            }
            if(isPieceAt(piece.getxPosition()+1, piece.getyPosition()+1) && !getPieceAt(piece.getxPosition() +1, piece.getyPosition()+1).isItWhite()){
                highLightEnemy(piece.getxPosition()+1,piece.getyPosition()+1 );
            }

        }else{
            if(piece.getyPosition() == 6){ //starting position for whites
                if(!isPieceAt(piece.getxPosition(), piece.getyPosition()-1)){
                    highlightSquare(piece.getxPosition(), piece.getyPosition()-1);
                    if(!isPieceAt(piece.getxPosition(), piece.getyPosition()-2)){
                        highlightSquare(piece.getxPosition(), piece.getyPosition()-2);
                    }
                }
            }else{
                //if there is one obstacle 1 square away
                if(!isPieceAt(piece.getxPosition(), piece.getyPosition()-1)){
                    highlightSquare(piece.getxPosition(), piece.getyPosition()-1);
                }

            }
            //if there are enemies one diagonal away
            if(isPieceAt(piece.getxPosition()-1, piece.getyPosition()-1) && getPieceAt(piece.getxPosition() -1, piece.getyPosition()-1).isItWhite()){ // have to also be the opposite colour
                highLightEnemy(piece.getxPosition()-1,piece.getyPosition()-1);
            }
            if(isPieceAt(piece.getxPosition()+1, piece.getyPosition()-1) && getPieceAt(piece.getxPosition() +1, piece.getyPosition()-1).isItWhite()){
                highLightEnemy(piece.getxPosition()+1,piece.getyPosition()-1 );
            }

        }
    }

    public void rookOptions(Piece piece){
        int xMarker;
        int yMarker;
        xMarker = piece.getxPosition();
        yMarker = piece.getyPosition();

        //scan downwards
        while(yMarker <= 7){ //scanning downward
            if(isPieceAt(xMarker, ++yMarker)){
                if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                    highLightEnemy(xMarker, yMarker);
                }
                break;
            }else{
                highlightSquare(xMarker, yMarker);
            }
        }
        xMarker = piece.getxPosition();
        yMarker = piece.getyPosition();
        while(yMarker >=0){
            if(isPieceAt(xMarker, --yMarker)){
                if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                    highLightEnemy(xMarker, yMarker);
                }
                break;
            }else{
                highlightSquare(xMarker, yMarker);
            }
        }
        xMarker = piece.getxPosition();
        yMarker = piece.getyPosition();
        while(xMarker >=0){
            if(isPieceAt(--xMarker, yMarker)){
                if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                    highLightEnemy(xMarker, yMarker);
                }
                break;
            }else{
                highlightSquare(xMarker, yMarker);
            }
        }
        xMarker = piece.getxPosition();
        yMarker = piece.getyPosition();
        while(xMarker <= 7){
            if(isPieceAt(++xMarker, yMarker)){
                if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                    highLightEnemy(xMarker, yMarker);
                }
                break;
            }else{
                highlightSquare(xMarker, yMarker);
            }
        }
    }

    public void bishopOptions(Piece piece){
        int xMarker;
        int yMarker;
        xMarker = piece.getxPosition();
        yMarker = piece.getyPosition();
        //down right
        while(xMarker <= 7 || yMarker <= 7){
            if(isPieceAt(++xMarker, ++yMarker)){
                if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                    highLightEnemy(xMarker, yMarker);
                }
                break;
            }else{
                highlightSquare(xMarker, yMarker);
            }
        }
        xMarker = piece.getxPosition();
        yMarker = piece.getyPosition();
        //down left
        while(xMarker >= 0 || yMarker <= 7){
            if(isPieceAt(--xMarker, ++yMarker)){
                if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                    highLightEnemy(xMarker, yMarker);
                }
                break;
            }else{
                highlightSquare(xMarker, yMarker);
            }
        }
        xMarker = piece.getxPosition();
        yMarker = piece.getyPosition();
        //up left
        while(xMarker >= 0 || yMarker >= 0){
            if(isPieceAt(--xMarker, --yMarker)){
                if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                    highLightEnemy(xMarker, yMarker);
                }
                break;
            }else{
                highlightSquare(xMarker, yMarker);
            }
        }
        xMarker = piece.getxPosition();
        yMarker = piece.getyPosition();
        //up right
        while(xMarker <= 7 || yMarker >= 0){
            if(isPieceAt(++xMarker, --yMarker)){
                if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                    highLightEnemy(xMarker, yMarker);
                }
                break;
            }else{
                highlightSquare(xMarker, yMarker);
            }
        }
    }

    public void queenOptions(Piece piece){
        rookOptions(piece);
        bishopOptions(piece);
    }

    public void kingOptions(Piece piece){
        int xMarker = piece.getxPosition();
        int yMarker = piece.getyPosition();

        if(!isPieceAt(xMarker+1, yMarker+1)){ //down right
            highlightSquare(xMarker+1, yMarker+1);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker+1, yMarker+1).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker+1, yMarker+1).isItWhite())){
                highLightEnemy(xMarker+1, yMarker+1);
            }
        }
        if(!isPieceAt(xMarker-1, yMarker-1)){ // up left
            highlightSquare(xMarker-1, yMarker-1);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker-1, yMarker-1).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker-1, yMarker-1).isItWhite())){
                highLightEnemy(xMarker-1, yMarker-1);
            }
        }
        if(!isPieceAt(xMarker+1, yMarker-1)){//up right
            highlightSquare(xMarker+1, yMarker-1);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker+1, yMarker-1).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker+1, yMarker-1).isItWhite())){
                highLightEnemy(xMarker+1, yMarker-1);
            }
        }
        if(!isPieceAt(xMarker-1, yMarker+1)){ //down left
            highlightSquare(xMarker-1, yMarker+1);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker-1, yMarker+1).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker-1, yMarker+1).isItWhite())){
                highLightEnemy(xMarker-1, yMarker+1);
            }
        }
        if(!isPieceAt(xMarker, yMarker+1)){ //down
            highlightSquare(xMarker, yMarker+1);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker, yMarker+1).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker+1).isItWhite())){
                highLightEnemy(xMarker, yMarker+1);
            }
        }
        if(!isPieceAt(xMarker, yMarker-1)){ //up
            highlightSquare(xMarker, yMarker-1);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker, yMarker-1).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker-1).isItWhite())){
                highLightEnemy(xMarker, yMarker-1);
            }
        }
        if(!isPieceAt(xMarker+1, yMarker)){ //right
            highlightSquare(xMarker+1, yMarker);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker+1, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker+1, yMarker).isItWhite())){
                highLightEnemy(xMarker+1, yMarker);
            }
        }
        if(!isPieceAt(xMarker-1, yMarker)){ // left
            highlightSquare(xMarker-1, yMarker);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker-1, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker-1, yMarker).isItWhite())){
                highLightEnemy(xMarker-1, yMarker);
            }
        }
    }

    public void knightOptions(Piece piece){
        int xMarker = piece.getxPosition();
        int yMarker = piece.getyPosition();


        yMarker = yMarker +2; //going downwards
        xMarker = xMarker+1; //going downwards and right
        if(!isPieceAt(xMarker, yMarker)){
            highlightSquare(xMarker, yMarker);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                highLightEnemy(xMarker, yMarker);
            }
        }
        xMarker = xMarker-2;
        if (!isPieceAt(xMarker, yMarker)){ //going down and left
            highlightSquare(xMarker, yMarker);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                highLightEnemy(xMarker, yMarker);
            }
        }
        xMarker--;
        yMarker--;
        if (!isPieceAt(xMarker, yMarker)){ //left and bottom
            highlightSquare(xMarker, yMarker);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                highLightEnemy(xMarker, yMarker);
            }
        }
        yMarker = yMarker-2;
        if (!isPieceAt(xMarker, yMarker)){ //left and upwards
            highlightSquare(xMarker, yMarker);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                highLightEnemy(xMarker, yMarker);
            }
        }
        xMarker++;
        yMarker--;
        if (!isPieceAt(xMarker, yMarker)){ //upwards and left
            highlightSquare(xMarker, yMarker);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                highLightEnemy(xMarker, yMarker);
            }
        }
        xMarker = xMarker+2;
        if (!isPieceAt(xMarker, yMarker)){ //upwards and right
            highlightSquare(xMarker, yMarker);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                highLightEnemy(xMarker, yMarker);
            }
        }
        xMarker++;
        yMarker++;
        if (!isPieceAt(xMarker, yMarker)){ //left and upwards
            highlightSquare(xMarker, yMarker);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                highLightEnemy(xMarker, yMarker);
            }
        }
        yMarker = yMarker+2;
        if (!isPieceAt(xMarker, yMarker)){ //left and downwards
            highlightSquare(xMarker, yMarker);
        }else{
            if((piece.isItWhite() && !getPieceAt(xMarker, yMarker).isItWhite()) || (!piece.isItWhite() && getPieceAt(xMarker, yMarker).isItWhite())){
                highLightEnemy(xMarker, yMarker);
            }
        }
    }

    public boolean isWhiteKingAlive(){
        for (int i = 0; i < whitePieces.size(); i++) {
            if(whitePieces.get(i).getType().equals("King")){
                return true;
            }
        }
        return false;
    }

    public boolean isBlackKingAlive(){
        for (int i = 0; i < blackPieces.size(); i++) {
            if(blackPieces.get(i).getType().equals("King")){
                return true;
            }
        }
        return false;
    }


}