package com.example.chess.domain;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.paint.Color;



public class Square {


    private int size;
    private Color color;
    private int xcoord;
    private int ycoord;

    public Square(int size, Color color, int xcoord, int ycoord){
        this.size = size;
        this.color = color;
        this.xcoord = xcoord;
        this.ycoord = ycoord;
    }

    public Color getColor(){
        return this.color;
    }
    public int getSize(){
        return size;
    }


}
