package com.example.chess.domain;

import javafx.scene.paint.Color;

public class Board {
    private int size;
    private Square[][] blackPieces;
    private Square[][] whitePieces;
    public Square[] squareList;

    public Board(int size){
        this.size = size;
        blackPieces = new Square[8][4];
        whitePieces = new Square[8][4];
        squareList = new Square[64];
    }
}
